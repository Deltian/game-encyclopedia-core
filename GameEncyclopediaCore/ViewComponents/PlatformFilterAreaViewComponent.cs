﻿using GameEncyclopediaCore.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.ViewComponents
{
    public class PlatformFilterAreaViewComponent : ViewComponent
    {
        private readonly IVideoGameService _videoGameService;

        public PlatformFilterAreaViewComponent(IVideoGameService videoGameService)
        {
            _videoGameService = videoGameService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var platformGroups = await _videoGameService.GetPlatformsAndCountVideoGames();

            return View(platformGroups);
        }
    }
}
