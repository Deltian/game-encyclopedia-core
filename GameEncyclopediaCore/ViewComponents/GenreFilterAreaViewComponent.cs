﻿using GameEncyclopediaCore.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.ViewComponents
{
    public class GenreFilterAreaViewComponent : ViewComponent
    {
        private readonly IVideoGameService _videoGameService;

        public GenreFilterAreaViewComponent(IVideoGameService videoGameService)
        {
            _videoGameService = videoGameService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var genres = await _videoGameService.GetGenresAndCountVideoGames();

            return View(genres);
        }
    }
}
