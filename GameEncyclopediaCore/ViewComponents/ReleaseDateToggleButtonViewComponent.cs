﻿using GameEncyclopediaCore.Resources.VideoGamesResources;
using GameEncyclopediaCore.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.ViewComponents
{
    public class ReleaseDateToggleButtonViewComponent : ViewComponent
    {
        private readonly IVideoGameService _videoGameService;

        public ReleaseDateToggleButtonViewComponent(IVideoGameService videoGameService)
        {
            _videoGameService = videoGameService;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool showGamesAfterRelease)
        {
            ViewBag.Count = await _videoGameService.CountVideoGames(g => showGamesAfterRelease ?
            g.ReleaseDate <= DateTime.Now :
            g.ReleaseDate > DateTime.Now);


            ViewBag.ButtonText = showGamesAfterRelease ? VideoGamesResources.AfterRelease : VideoGamesResources.BeforeRelease;

            return View();
        }
    }
}
