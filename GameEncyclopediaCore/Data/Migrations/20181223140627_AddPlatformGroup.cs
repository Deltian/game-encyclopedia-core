﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameEncyclopediaCore.Data.Migrations
{
    public partial class AddPlatformGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PlatformGroup",
                table: "Platforms",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlatformGroup",
                table: "Platforms");
        }
    }
}
