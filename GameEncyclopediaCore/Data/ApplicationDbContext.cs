﻿using System;
using System.Collections.Generic;
using System.Text;
using GameEncyclopediaCore.DomainModels;
using GameEncyclopediaCore.DomainModels.UserDomainModels;
using GameEncyclopediaCore.DomainModels.VideoGameDomainModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GameEncyclopediaCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<VideoGame> VideoGames { get; set; }

        public DbSet<VideoGameGenre> VideoGameGenres { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<VideoGamePlatform> VideoGamePlatforms { get; set; }

        public DbSet<Platform> Platforms { get; set; }

        public DbSet<UserLikedGame> UserLikedGames { get; set; }

        public DbSet<UserDislikedGame> UserDislikedGames { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Platform>().HasData(
                new Platform { Name="PC", PlatformGroup="PC" },
                new Platform { Name = "WWW", PlatformGroup = "PC" },
                new Platform { Name = "PS4", PlatformGroup = "Playstation" },
                new Platform { Name = "PS3", PlatformGroup = "Playstation" },
                new Platform { Name = "PS2", PlatformGroup = "Playstation" },
                new Platform { Name = "PSX", PlatformGroup = "Playstation" },
                new Platform { Name = "PSP", PlatformGroup = "Playstation" },
                new Platform { Name = "PS Vita", PlatformGroup = "Playstation" },
                new Platform { Name = "XBox", PlatformGroup = "XBox" },
                new Platform { Name = "XBox 360", PlatformGroup = "XBox" },
                new Platform { Name = "XBox One", PlatformGroup = "XBox" },
                 new Platform { Name = "Android", PlatformGroup = "Mobile" },
                 new Platform { Name = "iOS", PlatformGroup = "Mobile" },
                 new Platform { Name = "Windows Phone", PlatformGroup = "Mobile" },
                 new Platform { Name = "Switch", PlatformGroup = "Nintendo" },
                 new Platform { Name = "Wii", PlatformGroup = "Nintendo" },
                 new Platform { Name = "NDS", PlatformGroup = "Nintendo" },
                 new Platform { Name = "Wii U", PlatformGroup = "Nintendo" },
                 new Platform { Name = "3DS", PlatformGroup = "Nintendo" },
                 new Platform { Name = "GBA", PlatformGroup = "Nintendo" },
                 new Platform { Name = "GCN", PlatformGroup = "Nintendo" }
                );

            builder.Entity<Genre>().HasData(
                new Genre { Name="Action" },
                new Genre { Name = "Agility" },
                new Genre { Name = "Fights" },
                new Genre { Name = "RPG" },
                new Genre { Name = "Strategy" },
                new Genre { Name = "Adventure" },
                new Genre { Name = "Sports" },
                new Genre { Name = "Races" },
                new Genre { Name = "Simulations" },
                new Genre { Name = "Puzzle" },
                new Genre { Name = "Social" },
                new Genre { Name = "MMO" }
                );

            //builder.Entity<VideoGame>().HasData(
            //    new VideoGame
            //    {
            //        Id = Guid.NewGuid(),
            //        Title = "Crash Bandicoot N. Sane Trilogy",
            //        Description= "Kompilacja trzech pierwszych odsłon serii Crash Bandicoot, zremasterowanych przez studio Vicarious Visions. Crash Bandicoot N. Sane Trilogy na PC, PS4 itd. to pakiet trzech słynnych gier platformowych od studia Naughty Dog. W skład zestawu wchodzą: Crash Bandicoot, Cortex Strikes Back oraz Warped, przy czym wszystkie tytuły zostały zremasterowane przez studio Vicarious Visions, znane z Marvel: Ultimate Alliance 2, Skylanders SuperChargers i kilku innych projektów. Źródło: https://www.gry-online.pl/",
            //        Rating=7,
            //        ReleaseDate=new DateTime(2017, 6,30),
            //        VideoGameGenres=new List<VideoGameGenre>
            //        {
            //            new VideoGameGenre { GenreId="Agility" },
            //            new VideoGameGenre { GenreId="Adventure" }
            //        },
            //        VideoGamePlatforms=new List<VideoGamePlatform>
            //        {
            //            new VideoGamePlatform{ PlatformId="PS4" },
            //            new VideoGamePlatform{ PlatformId="PC" },
            //            new VideoGamePlatform{ PlatformId="Switch" },
            //            new VideoGamePlatform{ PlatformId="XBox One" }
            //        }
            //    });

            //builder.Entity<VideoGame>().HasData(
            //    new VideoGame
            //    {
            //        Id = Guid.NewGuid(),
            //        Title = "Assassin's Creed Origins",
            //        Description = "Kolejna odsłona bestsellerowego cyklu, stanowiąca niejako nowe otwarcie dla serii. Dzieło firmy Ubisoft przenosi graczy do starożytnego Egiptu, gdzie swoje początki miało bractwo tytułowych Asasynów.",
            //        Rating = 8.3F,
            //        ReleaseDate = new DateTime(2017, 10, 27),
            //        VideoGameGenres = new List<VideoGameGenre>
            //        {
            //            new VideoGameGenre { GenreId="Action" },
            //            new VideoGameGenre { GenreId="Adventure" }
            //        },
            //        VideoGamePlatforms = new List<VideoGamePlatform>
            //        {
            //            new VideoGamePlatform{ PlatformId="PS4" },
            //            new VideoGamePlatform{ PlatformId="PC" },
            //            new VideoGamePlatform{ PlatformId="XBox One" }
            //        }
            //    });

            //builder.Entity<VideoGame>().HasData(
            //    new VideoGame
            //    {
            //        Id=Guid.NewGuid(),
            //        Title = "Red Dead Redemption 2",
            //        Description = "Trzecia część serii Red Dead, a zarazem prequel bestsellerowego Red Dead Redemption. W Red Dead Redemption II gracze trafiają na Dziki Zachód z 1899, by w skórze Arthura Morgana stawić czoła problemom, jakie trapią gang Dutcha van der Lindego.",
            //        Rating = 9.3F,
            //        ReleaseDate = new DateTime(2018, 10, 30),
            //        VideoGameGenres = new List<VideoGameGenre>
            //        {
            //            new VideoGameGenre { GenreId="Action" },
            //            new VideoGameGenre { GenreId="Adventure" }
            //        },
            //        VideoGamePlatforms = new List<VideoGamePlatform>
            //        {
            //            new VideoGamePlatform{ PlatformId="PS4" },
            //            new VideoGamePlatform{ PlatformId="XBox One" }
            //        }
            //    });

            builder.Entity<VideoGameGenre>()
            .HasKey(_ => new { _.VideoGameId, _.GenreId });
            builder.Entity<VideoGameGenre>()
                .HasOne(vgg => vgg.VideoGame)
                .WithMany(vg => vg.VideoGameGenres)
                .HasForeignKey(vgg => vgg.VideoGameId);
            builder.Entity<VideoGameGenre>()
                .HasOne(vgg => vgg.Genre)
                .WithMany(g => g.VideoGameGenres)
                .HasForeignKey(vgg => vgg.GenreId);

            builder.Entity<VideoGamePlatform>()
            .HasKey(_ => new { _.VideoGameId, _.PlatformId });
            builder.Entity<VideoGamePlatform>()
                .HasOne(vgg => vgg.VideoGame)
                .WithMany(vg => vg.VideoGamePlatforms)
                .HasForeignKey(vgg => vgg.VideoGameId);
            builder.Entity<VideoGamePlatform>()
                .HasOne(vgg => vgg.Platform)
                .WithMany(p => p.VideoGamePlatforms)
                .HasForeignKey(vgg => vgg.PlatformId);

            builder.Entity<UserLikedGame>()
            .HasKey(_ => new { _.VideoGameId, _.UserId });
            builder.Entity<UserLikedGame>()
                .HasOne(ulg => ulg.VideoGame)
                .WithMany(vg => vg.UsersLikingGame)
                .HasForeignKey(ulg => ulg.VideoGameId);
            builder.Entity<UserLikedGame>()
                .HasOne(ulg => ulg.User)
                .WithMany(u => u.LikedGames)
                .HasForeignKey(ulg => ulg.UserId);

            builder.Entity<UserDislikedGame>()
            .HasKey(_ => new { _.VideoGameId, _.UserId });
            builder.Entity<UserDislikedGame>()
                .HasOne(udg => udg.VideoGame)
                .WithMany(vg => vg.UsersDislikingGame)
                .HasForeignKey(udg => udg.VideoGameId);
            builder.Entity<UserDislikedGame>()
                .HasOne(udg => udg.User)
                .WithMany(u => u.DislikedGames)
                .HasForeignKey(udg => udg.UserId);

            base.OnModelCreating(builder);
        }
    }
}
