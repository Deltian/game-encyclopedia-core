﻿using GameEncyclopediaCore.Data;
using GameEncyclopediaCore.DomainModels.VideoGameDomainModels;
using GameEncyclopediaCore.Models.VideoGameViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Services
{
    public class VideoGameService : IVideoGameService
    {
        private ApplicationDbContext _dbContext;

        public VideoGameService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<int> CountVideoGames(Func<VideoGame, bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var query = _dbContext.VideoGames.AsQueryable();

            var count = query.Count(predicate);

            return Task.FromResult(count);
        }

        public Task<IEnumerable<GenreToggleButtonViewModel>> GetGenresAndCountVideoGames()
        {
            var genres = _dbContext.Genres.
                Select(p => new GenreToggleButtonViewModel { Genre = p.Name, VideoGameCount = p.VideoGameGenres.Count() })
                .ToList().AsEnumerable();

            return Task.FromResult(genres);
        }

        public Task<IEnumerable<PlatformToggleButtonGroupViewModel>> GetPlatformsAndCountVideoGames()
        {
           var platformGroups= _dbContext.Platforms.
                Select(p => new PlatformToggleButtonViewModel { Platform = p.Name, VideoGameCount = p.VideoGamePlatforms.Count(), PlatformGroup=p.PlatformGroup })
                .GroupBy(p => p.PlatformGroup).ToList().Select(g => new PlatformToggleButtonGroupViewModel { PlatformGroup=g.Key, Platforms=g });

            return Task.FromResult(platformGroups);
        }

        public Task<List<ListVideoGameViewModel>> GetVideoGamesBy(SearchVideoGamesViewModel model)
        {
            var videoGamesQuery = from vg in _dbContext.VideoGames.Include(vg => vg.VideoGameGenres).Include(vg => vg.VideoGamePlatforms)
                                  where vg.Title.Contains(model.SearchQuery, StringComparison.InvariantCultureIgnoreCase)
                                  && vg.ReleaseDate.Year >= model.MinYear && vg.ReleaseDate.Year <= model.MaxYear
                                  && model.DisplayBeforeOrAfterRelease == DisplayBeforeOrAfterRelease.AfterRelease ? vg.ReleaseDate <= DateTime.Now :
                                  model.DisplayBeforeOrAfterRelease == DisplayBeforeOrAfterRelease.BeforeRelease ? vg.ReleaseDate > DateTime.Now :
                                  true && vg.VideoGameGenres.Any(g => model.Genres.Contains(g.GenreId)) && vg.VideoGamePlatforms.Any(p => model.Platforms.Contains(p.PlatformId))
                                  select new ListVideoGameViewModel { Title=vg.Title, Description=vg.Description, Id=vg.Id,
                                      ReleaseDate =vg.ReleaseDate, Platforms=vg.VideoGamePlatforms.Select(p => p.PlatformId),
                                      Genres=vg.VideoGameGenres.Select(p => p.GenreId) };

            var results = videoGamesQuery.ToList();

            return Task.FromResult(results);
        }
    }
}
