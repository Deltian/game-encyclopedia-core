﻿using GameEncyclopediaCore.DomainModels.VideoGameDomainModels;
using GameEncyclopediaCore.Models.VideoGameViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Services
{
    public interface IVideoGameService
    {
        Task<int> CountVideoGames(Func<VideoGame, bool> predicate);

        Task<IEnumerable<PlatformToggleButtonGroupViewModel>> GetPlatformsAndCountVideoGames();

        Task<IEnumerable<GenreToggleButtonViewModel>> GetGenresAndCountVideoGames();

        Task<List<ListVideoGameViewModel>> GetVideoGamesBy(SearchVideoGamesViewModel model);
    }
}
