﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Utils
{
    public static class HtmlHelpers
    {
        public static string AddActiveClass(string currentAction, string actionOnNavbar)
        {
            return currentAction == actionOnNavbar ? " active" : "";
        }
    }
}
