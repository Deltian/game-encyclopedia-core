﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Utils.ValidationAttributes
{
    public class YearRangeAttribute : RangeAttribute
    {
        public YearRangeAttribute(int minimum, int addYearsFromNow) : base(minimum, DateTime.Now.AddYears(addYearsFromNow).Year)
        {
        }
    }
}
