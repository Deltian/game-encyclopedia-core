﻿using GameEncyclopediaCore.Resources.AccountResources;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Utils
{
    public static class DropDownLists
    {
        public static IEnumerable<SelectListItem> Voivodeships()
        {
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem(AccountResource.Select, ""));
            list.Add(new SelectListItem(AccountResource.LowerSilesia, nameof(AccountResource.LowerSilesia)));
            list.Add(new SelectListItem(AccountResource.GreaterPoland, nameof(AccountResource.GreaterPoland)));
            list.Add(new SelectListItem(AccountResource.Silesia, nameof(AccountResource.Silesia)));

            return list;
        }
    }
}
