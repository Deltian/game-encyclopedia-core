﻿function showErrorAlert(xhr, status, error) {
    var template = $('<div class="alert alert-danger alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

    template.append(serverVariables.errorOccured);

    $("#alertBox").html(template);

    setTimeout(function () {
        $("#alertBox>div").alert('close');
    }, 2000);
}

function showSuccessAlert(data, status, xhr) {
    setTimeout(function () {
        $("#alertBox>div").alert('close');
    }, 2000);
}