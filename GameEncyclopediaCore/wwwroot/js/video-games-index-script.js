﻿var maxYear = parseInt($("#maxYear").val());
var minYear = parseInt($("#minYear").val());

$("#yearRangeSlider").slider({
    animate: "fast",
    max: maxYear,
    min: minYear,
    range: true,
    values: [minYear, maxYear],
    step: 1,
    change: function (event, ui) {
        if (ui.handleIndex === 0)
            $("#minYear").val(ui.value);
        else
            $("#maxYear").val(ui.value);
    }
});