﻿using System;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class VideoGamePlatform
    {
        public Guid VideoGameId { get; set; }

        public VideoGame VideoGame { get; set; }

        public string PlatformId { get; set; }

        public Platform Platform { get; set; }
    }
}