﻿using System;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class VideoGameGenre
    {
        public Guid VideoGameId { get; set; }

        public VideoGame VideoGame { get; set; }

        public string GenreId { get; set; }

        public Genre Genre { get; set; }
    }
}