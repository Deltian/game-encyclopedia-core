﻿using GameEncyclopediaCore.DomainModels.UserDomainModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class UserDislikedGame
    {
        public string UserId { get; set; }

        public User User { get; set; }

        public Guid VideoGameId { get; set; }

        public VideoGame VideoGame { get; set; }
    }
}
