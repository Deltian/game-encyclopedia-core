﻿using GameEncyclopediaCore.DomainModels.UserDomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class UserLikedGame
    {
        public string UserId { get; set; }

        public User User { get; set; }

        public Guid VideoGameId { get; set; }

        public VideoGame VideoGame { get; set; }
    }
}
