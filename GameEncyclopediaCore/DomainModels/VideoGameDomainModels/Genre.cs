﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class Genre
    {
        [Key]
        public string Name { get; set; }

        public IEnumerable<VideoGameGenre> VideoGameGenres { get; set; }
    }
}
