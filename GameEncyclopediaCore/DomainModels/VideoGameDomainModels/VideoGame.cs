﻿using GameEncyclopediaCore.DomainModels.UserDomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.DomainModels.VideoGameDomainModels
{
    public class VideoGame
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string Description { get; set; }

        public float Rating { get; set; }

        public IEnumerable<VideoGameGenre> VideoGameGenres { get; set; }

        public IEnumerable<VideoGamePlatform> VideoGamePlatforms { get; set; }

        public IEnumerable<Comment> Comments { get; set; }

        public IEnumerable<UserLikedGame> UsersLikingGame { get; set; }

        public IEnumerable<UserDislikedGame> UsersDislikingGame { get; set; }
    }
}
