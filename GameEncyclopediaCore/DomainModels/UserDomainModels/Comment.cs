﻿using GameEncyclopediaCore.DomainModels.VideoGameDomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.DomainModels.UserDomainModels
{
    public class Comment
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public Guid VideoGameId { get; set; }

        public VideoGame VidoeGame { get; set; }
    }
}
