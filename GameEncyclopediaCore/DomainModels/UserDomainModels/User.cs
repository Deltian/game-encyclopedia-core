﻿using GameEncyclopediaCore.DomainModels.VideoGameDomainModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameEncyclopediaCore.DomainModels.UserDomainModels
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string Street { get; set; }

        public string HouseNumber { get; set; }

        public int? AptNumber { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string Voivodeship { get; set; }

        public IEnumerable<Comment> Comments { get; set; }

        public IEnumerable<UserLikedGame> LikedGames { get; set; }

        public IEnumerable<UserDislikedGame> DislikedGames { get; set; }
    }
}