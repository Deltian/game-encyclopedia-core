﻿using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class MyAccountViewModel
    {
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = nameof(AccountDisplayNameResource.FirstName))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = nameof(AccountDisplayNameResource.LastName))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "BirthDate")]
        [DataType(DataType.Date, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidDateFormat))]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "HouseNumber")]
        public string HouseNumber { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "AptNumber")]
        public int? AptNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "PostalCode")]
        [RegularExpression(@"^\d{2}-\d{3}$")]
        public string PostalCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Voivodeship")]
        public string Voivodeship { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "PhoneNumber")]
        [DataType(DataType.PhoneNumber, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidPhoneNumberFormat))]
        public string PhoneNumber { get; set; }
    }
}
