﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using GameEncyclopediaCore.Resources;
using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Display(ResourceType =typeof(AccountDisplayNameResource), Name ="UserName")]
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName ="Required")]
        public string Username { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Password")]
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "RememberMe")]
        public bool RememberMe { get; set; }
    }
}