﻿using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class UserNameViewModel
    {
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = "Required")]
        [StringLength(12, MinimumLength = 6, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "UserName")]
        public string UserName { get; set; }
    }
}
