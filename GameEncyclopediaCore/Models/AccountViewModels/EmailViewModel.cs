﻿using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class EmailViewModel
    {
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [EmailAddress(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidEmailAddressFormat))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
