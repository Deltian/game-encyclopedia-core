﻿using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class PasswordViewModel
    {
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "OldPassword")]
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])(?=.*\W).{1,}$", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.ValidatePassword))]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "NewPassword")]
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])(?=.*\W).{1,}$", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.ValidatePassword))]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "ConfirmPassword")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.PasswordMismatch))]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
