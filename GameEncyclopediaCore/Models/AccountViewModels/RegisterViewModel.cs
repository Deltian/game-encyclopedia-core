﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using GameEncyclopediaCore.Resources.AccountResources;
using GameEncyclopediaCore.Resources.CommonResources;

namespace GameEncyclopediaCore.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [StringLength(12, MinimumLength = 6, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [EmailAddress(ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidEmailAddressFormat))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name ="Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "Password")]
        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])(?=.*\W).{1,}$", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName =nameof(AccountValidationResource.ValidatePassword))]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceType =typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.StringLength))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.PasswordMismatch))]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(CommonValidationResources), ErrorMessageResourceName = nameof(CommonValidationResources.Required))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = "BirthDate")]
        [DataType(DataType.Date, ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.InvalidDateFormat))]
        public DateTime? BirthDate { get; set; }

        //[Range(typeof(bool), "True", "True", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.AcceptRule))]
        [Display(ResourceType = typeof(AccountDisplayNameResource), Name =nameof(AccountDisplayNameResource.AcceptFirstRule))]
        public bool AcceptFirstRule { get; set; }

        [Display(ResourceType = typeof(AccountDisplayNameResource), Name = nameof(AccountDisplayNameResource.AcceptSecondRule))]
        //[Range(typeof(bool), "True", "True", ErrorMessageResourceType = typeof(AccountValidationResource), ErrorMessageResourceName = nameof(AccountValidationResource.AcceptRule))]
        public bool AcceptSecondRule { get; set; }
    }
}