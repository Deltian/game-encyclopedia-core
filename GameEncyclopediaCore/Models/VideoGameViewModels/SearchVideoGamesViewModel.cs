﻿using GameEncyclopediaCore.Utils.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.VideoGameViewModels
{
    public class SearchVideoGamesViewModel : IValidatableObject
    {
        public string SearchQuery { get; set; }

        [YearRange(1960, 1, ErrorMessage = "The year must not be less than {1} and greater than {2}")]
        public int MinYear { get; set; }

        [YearRange(1960, 1, ErrorMessage = "The year must not be less than {1} and greater than {2}")]
        public int MaxYear { get; set; }

        public DisplayBeforeOrAfterRelease DisplayBeforeOrAfterRelease { get; set; }

        public string[] Platforms { get; set; }

        public string[] Genres { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if (MinYear <= MaxYear)
                yield return ValidationResult.Success;

            yield return new ValidationResult("Maximum year may not be less than minimum year");
        }
    }

    public enum DisplayBeforeOrAfterRelease
    {
        All,
        AfterRelease,
        BeforeRelease
    }
}
