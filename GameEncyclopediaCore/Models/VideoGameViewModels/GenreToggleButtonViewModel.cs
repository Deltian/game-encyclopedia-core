﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.VideoGameViewModels
{
    public class GenreToggleButtonViewModel
    {
        public string Genre { get; set; }

        public int VideoGameCount { get; set; }
    }
}
