﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.VideoGameViewModels
{
    public class PlatformToggleButtonGroupViewModel
    {
        public string PlatformGroup { get; set; }

        public IEnumerable<PlatformToggleButtonViewModel> Platforms { get; set; }
    }
}
