﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.VideoGameViewModels
{
    public class ListVideoGameViewModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string Description { get; set; }

        public IEnumerable<string> Genres { get; set; }

        public IEnumerable<string> Platforms { get; set; }
    }
}
