﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.Models.VideoGameViewModels
{
    public class PlatformToggleButtonViewModel
    {
        public string Platform { get; set; }

        public int VideoGameCount { get; set; }

        public string PlatformGroup { get; set; }
    }
}
