﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEncyclopediaCore.Models.VideoGameViewModels;
using GameEncyclopediaCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameEncyclopediaCore.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideoGamesController : ControllerBase
    {
        private readonly IVideoGameService _videoGameService;

        public VideoGamesController(IVideoGameService videoGameService)
        {
            _videoGameService = videoGameService;
        }

        public async Task<IActionResult> GetVideoGames(SearchVideoGamesViewModel model)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            IEnumerable<ListVideoGameViewModel> videoGames = await _videoGameService.GetVideoGamesBy(model);

            return new JsonResult(videoGames);
        }
    }
}