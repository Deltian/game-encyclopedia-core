﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using GameEncyclopediaCore.DomainModels;
using GameEncyclopediaCore.DomainModels.UserDomainModels;
using GameEncyclopediaCore.Models.AccountViewModels;
using GameEncyclopediaCore.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace GameEncyclopediaCore.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        [NonAction]
        public async  Task<IActionResult> UpdateUser<TViewModel>(TViewModel model)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            _mapper.Map(model, user);

            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                ViewBag.Errors = result.Errors.Select(e => e.Description);
            }

            return PartialView("_AlertPartial");
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            ViewBag.CurrentAction = "Index";

            return View();
        }

        [Authorize]
        public async Task<IActionResult> MyAccount()
        {
            ViewBag.CurrentAction = "MyAccount";

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var firstModel = _mapper.Map<MyAccountViewModel>(user);

            ViewData["userNameViewModel"] = _mapper.Map<UserNameViewModel>(user);

            ViewData["emailViewModel"] = _mapper.Map<EmailViewModel>(user);

            return View(firstModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, false);

            if (result.Succeeded)
                return LocalRedirect(returnUrl);
            else if (result.IsNotAllowed)
                return LocalRedirect(returnUrl);

            return UnprocessableEntity();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            var user = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                BirthDate = model.BirthDate.Value
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);

                return Redirect(returnUrl);
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return Redirect(returnUrl);
            }
        }

        [Ajax]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> UpdateAccount(MyAccountViewModel model)
        {
            return await UpdateUser(model);
        }

        [Ajax]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> UpdateUserName(UserNameViewModel model)
        {
            return await UpdateUser(model);
        }

        [Ajax]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> UpdateEmail(EmailViewModel model)
        {
            return await UpdateUser(model);
        }

        public async Task<IActionResult> UpdatePassword(PasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                ViewBag.Errors = result.Errors.Select(e => e.Description);
            }

            return PartialView("_AlertPartial");
        }
    }

    

    public enum AccountIndexCard
    {
        MyAccount,
        UserName,
        Email,
        Password
    }
}