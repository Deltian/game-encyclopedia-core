﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameEncyclopediaCore.Models.VideoGameViewModels;
using GameEncyclopediaCore.Services;
using GameEncyclopediaCore.Utils;
using Microsoft.AspNetCore.Mvc;

namespace GameEncyclopediaCore.Controllers
{
    public class VideoGamesController : Controller
    {
        private readonly IVideoGameService _videoGameService;

        public VideoGamesController(IVideoGameService videoGameService)
        {
            _videoGameService = videoGameService;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.CountAfterRelease = await _videoGameService.CountVideoGames(g => g.ReleaseDate <= DateTime.Now);

            ViewBag.CountBeforeRelease = await _videoGameService.CountVideoGames(g => g.ReleaseDate <= DateTime.Now);

            return View();
        }
    }
}