﻿using AutoMapper;
using GameEncyclopediaCore.DomainModels;
using GameEncyclopediaCore.DomainModels.UserDomainModels;
using GameEncyclopediaCore.Models.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameEncyclopediaCore.AutomapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, MyAccountViewModel>();
            CreateMap<User, EmailViewModel>();
            CreateMap<User, UserNameViewModel>();
            CreateMap<User, PasswordViewModel>();

            CreateMap<MyAccountViewModel, User>();
            CreateMap<UserNameViewModel, User>();
            CreateMap<EmailViewModel, User>();
            CreateMap<PasswordViewModel, User>();
        }
    }
}
