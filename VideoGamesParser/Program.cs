﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace VideoGamesParser
{
    class Program
    {
        static void Main(string[] args)
        {
            var addVideoGameCodeTemplate = "builder.Entity<VideoGame>().HasData(\nnew VideoGame\n{\nId = Guid.NewGuid(),\nTitle = \"{0}\"\nDescription = \"{1}\"\nRating= {2}F\nReleaseDate=new DateTime({3},{4},{5})\nVideoGameGenres=new List<VideoGameGenre>\n{\nnew VideoGameGenre { GenreId={6} }\n},\nVideoGamePlatforms=new List<VideoGamePlatform>\n{\n{5}}\n});";

            var addPlatformCodeTemplate = "new VideoGamePlatform { PlatformId ={0} }\n";

            var sb = new StringBuilder();
            //@"Id = Guid.NewGuid(),
            //        Title = ,
            //        Description= ,
            //        Rating=7,
            //        ReleaseDate=new DateTime(2017, 6,30),
            //        VideoGameGenres=new List<VideoGameGenre>
            //        {
            //            new VideoGameGenre { GenreId= },
            //           new VideoGameGenre { GenreId= }
            //       },
            //        VideoGamePlatforms=new List<VideoGamePlatform>
            //        {
            //           new VideoGamePlatform{ PlatformId= },
            //            new VideoGamePlatform{ PlatformId= },
            //            new VideoGamePlatform{ PlatformId= },
            //            new VideoGamePlatform{ PlatformId= }
            //        }
            //    });"

            var genreMappings = new Dictionary<string, string>
            {
                { "Akcji", "Action" },
                {"Zręcznościowe", "Agility" },
                {"Bijatyki", "Fights" },
                {"RPG", "RPG" },
                {"Strategiczne", "Strategy" },
                {"Przygodowe", "Adventure" },
                { "Sportowe","Sports" },
                { "Wyścigi", "Races" },
                { "Symulacje", "Simulations" },
                { "Logiczne", "Puzzle" },
                {"Towarzyskie", "Social" },
                { "MMO", "MMO" }
            };

            var platformMappings = new Dictionary<string, string>
            {
                {"PS1", "PSX" },
                {"PSV","PS Vita" },
                {"XBOX", "XBox" },
                { "X360", "XBox 360"},
                { "XONE", "XBox One"},
                {"AND", "Android" },
                {"WP", "Windows Phone"},
                {"WiiU", "Wii U" }
            };

            Console.WriteLine("Start parsing Video Games from Gry-Online");

            var web = new HtmlWeb();

            for (int y = 1980; y <= 2019; y++)
            {
                var pageWithGames = web.Load($"https://www.gry-online.pl/S015.asp?ROK={y}");

                Console.WriteLine($"Parsing games from {y}");

                var gameBoxes = pageWithGames.DocumentNode.SelectSingleNode("div[@class='lista-gry']").ChildNodes;

                var gameUrls = gameBoxes.Select(n => n.FirstChild.Attributes["href"].Value);

                foreach (var gameUrl in gameUrls)
                {
                    var gamePage = web.Load(gameUrl).DocumentNode;

                    Console.WriteLine($"Parsing game from url {gameUrl}");

                    var title = gamePage.SelectSingleNode("[@id='game-title-cnt']").InnerText.Trim();

                    var description = gamePage.SelectSingleNode("[@id='game-description-cnt']").InnerHtml.Trim();

                    var ratingText = gamePage.SelectSingleNode("div[@class='score']").InnerText;

                    var rating = float.Parse(ratingText);

                    var singlePlatform = gamePage.SelectSingleNode("[@id='game-platform-cnt']").InnerText.Trim();

                    string[] manyPlatforms = null;

                    var platformsPanel = gamePage.SelectSingleNode("div[@class='S016-grupa-platformy']");

                    if (platformsPanel != null)
                        manyPlatforms = platformsPanel
                            .ChildNodes.Select(n => n.Attributes["title"].Value.Trim()).ToArray();
                    else
                        manyPlatforms = new[] { singlePlatform };

                    for (int index = 0; index< manyPlatforms.Length; )
                    {
                        var platform = manyPlatforms[index];

                        manyPlatforms[index]= platformMappings[platform];
                    }

                    var firstReleaseDateNode = gamePage.SelectSingleNode("[@id='game-dates-cnt']").FirstChild.ChildNodes.First(cn => cn.HasClass("multi-p"));

                    var dateElements = firstReleaseDateNode.FirstChild.ChildNodes.Select(cn => cn.InnerText.Trim()).ToArray();

                    var dateText = $"{dateElements[0]} {dateElements[1]} {dateElements[2]}";

                    var date = DateTime.Parse(dateText, CultureInfo.CurrentCulture);

                    var genrePolish= gamePage.SelectSingleNode("[@class='tagi']").FirstChild.FirstChild.InnerText.Trim();

                    var effectiveGenre = genreMappings[genrePolish];

                    Console.WriteLine($"Following video game has been parsed: {title}");
                    Console.WriteLine($"Description: {description.Substring(0, 20)}");
                    Console.WriteLine($"Release date: {date}");
                    Console.WriteLine($"Rating: {rating}");
                    Console.WriteLine("Platforms:");
                    foreach (var platform in manyPlatforms)
                    {
                        Console.WriteLine(platform);
                    }
                    Console.WriteLine("Genres:");
                    Console.WriteLine(effectiveGenre);

                    Console.WriteLine("Saving the data above to txt file");

                    using (var textWriter = new StreamWriter(@"C:\Users\Deltian\Documents\Visual Studio 2017\Projects\GameEncyclopediaCore\VideoGamesParser"))
                    {

                    }
                }
            }
        }
    }
}
